extends Node

var current_map : RandomMap
var MINING_CLASS : PackedScene = preload("res://scenes/level/Mining.tscn")

var MINING_TILEMAP_SIZE : Vector2 = Vector2(30,20)

func start_mining():
	var mining_scene = MINING_CLASS.instance()
	if Global.current_mode == Global.State.MINING:
		return
	current_map = Global.game.get_current_map()
	Global.game.save_map()
	
	# get player position in cells
	var pos = Global.current_map.get_position_in_cells(Player.node.position) - MINING_TILEMAP_SIZE/2
	var rec : Rect2 = Rect2(pos, MINING_TILEMAP_SIZE)
	
	var foreground = current_map.get_region_foreground(rec)
	var background = current_map.get_region_background(rec)
	mining_scene.set_map(foreground, background)
	
	Global.free_scene = false
	Global.change_scene_to_mining(mining_scene)
	print("start mining")
	Global.current_mode = Global.State.MINING
	
	Global.base_camera.zoom -= Vector2(0.25, 0.25)
	
func stop_mining():
	Global.game.restore_map()
	Global.current_map = Global.game.get_current_map()
	Global.game.set_ui_state(Global.StateUI.MAP)
	print("stop mining")
	Global.current_mode = Global.State.ON_MAP
	Global.base_camera.zoom += Vector2(0.25, 0.25)
	
