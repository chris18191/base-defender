shader_type canvas_item;

void vertex(){
	VERTEX.x += cos(TIME + VERTEX.x + VERTEX.y)* 10.0;
	VERTEX.y += sin(TIME + VERTEX.y + VERTEX.x)* 10.0;
}


void fragment(){
	vec2 uv = UV;
	
	vec4 color = texture(TEXTURE, uv);
	
	vec4 diff = vec4(1.0)-color;
	if (diff.r < .7 && diff.g<.65 && diff.b<.5){
		color.r = sin(TIME*2.0)*uv.x*2.0;
		color.b = sin(TIME*1.0)*uv.y*2.0;
	}
	
	COLOR = color;
}