# Base Defender
_a project template for the Godot engine_

## Objects

### Base
A movable object which holds a set of towers and other features to defend itself from enemies.

### Tower
Building on top of the base, which attack nearby enemies automatically.

### Enemy
Spawn random on the map or around the base. They try to destroy the base.

### Map
Defines the area in which the base can move around. Can have different sublevels e.g. other maps that can be reached from it.