extends Node

class_name Game

onready var _map_container = $Map
onready var _player_container = $Player
onready var _ui : CanvasLayer = $Interface
onready var _transition : CanvasLayer = $Transition


onready var _ui_main_menu : Control = preload("res://scenes/gui/MainMenu.tscn").instance()
onready var _ui_ingame : Control = preload("res://scenes/gui/GUI_InGame.tscn").instance()
onready var _ui_mining : Control = preload("res://scenes/gui/GUI_Mining.tscn").instance()
onready var _ui_hud : Control = preload("res://scenes/gui/HUD.tscn").instance()

var _current_map : Map
var _last_map : Map
var _last_base_position : Vector2

onready var _player_base : Base


func _input(_event):
	if Input.is_action_just_pressed("toggle_mining"):
		if Global.current_mode == Global.State.MINING:
			GlobalMining.stop_mining()
		else:
			GlobalMining.start_mining()
	elif Input.is_action_just_pressed("startMining"):
		GlobalMining.start_mining()
	elif Input.is_action_just_pressed("stopMining"):
		GlobalMining.stop_mining()

func _ready():
	Global.game = self
	_player_base = Player.node
	if _ui_main_menu.connect("start_game", self, "_on_MainMenu_start_game"):
		print_debug("could not connect main menu")
	_ui.add_child(_ui_main_menu)

func set_ui_state(state):
	# reset all ui to add only the correct interface nodes
	clear_ui()
	match state:
		Global.StateUI.MAIN_MENU:
			_ui.add_child(_ui_main_menu)
		Global.StateUI.MAP:
			_ui.add_child(_ui_ingame)
			_ui.add_child(_ui_hud)
		Global.StateUI.MINING:
			_ui.add_child(_ui_mining)

func clear_ui():
	for child in _ui.get_children():
		_ui.remove_child(child)

func clear_map():
	for child in _map_container.get_children():
		_map_container.remove_child(child)

func free_map():
	clear_map()

func get_current_map()-> Map:
	return _current_map

func set_map(map : Node):
	clear_map()
	_map_container.add_child(map)
	_current_map = map
	if not _player_base in _player_container.get_children():
		_player_container.add_child(_player_base)
	_player_base.position = map.get_spawn().position

func add_transition(trans : Transition):
	_transition.add_child(trans)
	
func remove_transition(trans : Transition):
	_transition.remove_child(trans)
	

func add_interface(ui : Node):
	if not ui in _ui.get_children():
		_ui.add_child(ui)

func _on_MainMenu_start_game():
	_ui.remove_child(_ui_main_menu)
	Global.change_scene_to_map("res://scenes/level/RandomMap.tscn")

func save_map():
	_last_map = _current_map
	_last_base_position = _player_base.position

func restore_map():
	clear_map()
	_current_map.queue_free()
	_map_container.add_child(_last_map)
	_current_map = _last_map
	_player_base.position = _last_base_position
	_last_map = null
