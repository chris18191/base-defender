extends Node2D

var Room = preload("res://tools/MapGenerator_Room.tscn")
onready var Map = $TileMap

export (float, 0, 1) var decorationProb
export (Array, int) var DECORATIONS = []
export (int) var GROUND
export (int) var WALL

export (int) var tile_size = 16
export (int) var hspread = 300
export (int) var num_rooms = 10
export (int) var min_size = 5
export (int) var max_size = 10

var rng:RandomNumberGenerator = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	generate_rooms()

func generate_rooms():
	print("MG: generateRooms")
	for i in range(num_rooms):
		var r = Room.instance()
		var size = Vector2 (0,0)
		var pos = Vector2 (rng.randi_range(-hspread, hspread), 0)
		size.x = min_size + rng.randi() % (max_size - min_size)
		size.y = min_size + rng.randi() % (max_size - min_size)
		r.make_room(pos, size * tile_size)
		$Rooms.add_child(r)

func _draw():
	if not get_parent().is_class("Viewport"):
		return
	for room in $Rooms.get_children():
		draw_rect(Rect2(room.position - room.size, room.size*2),
					Color(120, 40,80, 0.5), false)

func _process(delta):
	update()

func generate(map:TileMap, tree:SceneTree):
	# callable from the outside
	Map = map
	Map.clear()
	generate_rooms()
	yield(tree.create_timer(1), "timeout")
	generate_tilemap()
	return Map

func _input(event):
	if not get_parent().is_class("Viewport"):
		return
	if event.is_action_pressed("ui_select"):
		for child in $Rooms.get_children():
			child.queue_free()
		Map.clear()
		generate_rooms()
	if event.is_action_pressed("ui_right"):
		generate_tilemap()
	if event.is_action_pressed("save"):
		var rect = Rect2(get_viewport_rect().position / 2, get_viewport_rect().size/2)
		$CanvasLayer/SaveDialog.popup(rect)
	if event is InputEventMouseButton and event.is_pressed():
		if event.button_index == BUTTON_WHEEL_UP:
			$Camera2D.zoom += Vector2(0.5, 0.5)
		if event.button_index == BUTTON_WHEEL_DOWN:
			if $Camera2D.zoom >= Vector2(1,1):
				$Camera2D.zoom -= Vector2(0.5, 0.5)
		
func generate_tilemap():
	Map.clear()
	for room in $Rooms.get_children():
		room.mode = RigidBody2D.MODE_STATIC
	for room in $Rooms.get_children():
		var s = (room.size / tile_size).floor()
		var pos = Map.world_to_map(room.position)
		var ul = (room.global_position/tile_size) - s
		# genewrate inner
		generate_inner(ul, s*2)
		# generate wall
		generate_wall(ul, s*2)
	Map.update_bitmask_region()

func generate_wall(pos, size):
	var px = 0
	var py = 0
	for x in range(0,size.x+1):
		px = pos.x + x
		py = pos.y
		if Map.get_cell(px, py) == TileMap.INVALID_CELL:
			Map.set_cell(px, py, WALL)
		elif Map.get_cell(px, py) == WALL and count_neighbours(px, py, GROUND) >3:
			Map.set_cell(px, py, GROUND)
		
		px = pos.x + x
		py = pos.y + size.y
		if Map.get_cell(px, py) == TileMap.INVALID_CELL:
			Map.set_cell(px, py, WALL)
		elif Map.get_cell(px, py) == WALL and count_neighbours(px, py, GROUND) >3:
			Map.set_cell(px, py, GROUND)
		
	for y in range(0,size.y):		
		px = pos.x
		py = pos.y + y
		if Map.get_cell(px, py) == TileMap.INVALID_CELL:
			Map.set_cell(px, py, WALL)
		elif Map.get_cell(px, py) == WALL and count_neighbours(px, py, GROUND) >3:
			Map.set_cell(px, py, GROUND)
		
		px = pos.x + size.x
		py = pos.y + y
		if Map.get_cell(px, py) == TileMap.INVALID_CELL:
			Map.set_cell(px, py, WALL)
		elif Map.get_cell(px, py) == WALL and count_neighbours(px, py, GROUND) >3:
			Map.set_cell(px, py, GROUND)

func count_neighbours(x:int, y:int, tile:int) -> int:
	var count = 0

	if Map.get_cell(x-1,y-1) == tile:
		count += 1
	if Map.get_cell(x-1,y) == tile:
		count += 1
	if Map.get_cell(x,y-1) == tile:
		count += 1
	if Map.get_cell(x+1,y) == tile:
		count += 1
	if Map.get_cell(x,y+1) == tile:
		count += 1
	if Map.get_cell(x+1,y+1) == tile:
		count += 1
	if Map.get_cell(x-1,y+1) == tile:
		count += 1
	if Map.get_cell(x+1,y-1) == tile:
		count += 1

	return count

func generate_inner(pos, size):
	for x in range(1,size.x):
		for y in range(1, size.y):
			Map.set_cell(pos.x + x, pos.y + y, GROUND)


func _on_SaveDialog_file_selected(path:String):
	if not path.ends_with(".tres"):
		path += ".tscn"
	print(path)
	var packed_map = PackedScene.new()
	packed_map.pack($TileMap)
	ResourceSaver.save(path, packed_map)
