extends Node2D

export (Vector2) var tileSize = Vector2(32, 32)
export (Vector2) var spacing = Vector2(0,0)
export (String) var outputFile = "assets/tileset"

onready var texture = $Sprite.texture

func _ready():
	var tileCount = Vector2(ceil(texture.get_width()/(tileSize.x+spacing.x)), ceil(texture.get_height()/(tileSize.y+spacing.y)))
	var ts = TileSet.new()
	print(tileCount)
	
	for i in range(tileCount.x):
		for j in range(tileCount.y):
			var region = Rect2(i*(tileSize.x+spacing.x), j*(tileSize.y+spacing.y), tileSize.x, tileSize.y)
			var id = i + j*100
			
			ts.create_tile(id)
			ts.tile_set_texture(id, texture)
			ts.tile_set_region(id, region)
	ResourceSaver.save("res://" +outputFile+ ".tres", ts)
	print("successfully saved res://" +outputFile+ ".tres")
