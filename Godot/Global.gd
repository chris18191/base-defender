extends Node

enum State {NONE, ON_MAP, MINING}
enum StateUI {NONE, MAIN_MENU, MAP, MINING}

var current_mode = State.NONE

var root

var current_map = null
var current_map_scene = null
var current_scene = null

var next_scene = null
var next_ui_state : int = 0

onready var transition_scene = preload("res://scenes/gui/Transition.tscn")

# saves wether the current scene should be deleted when switching scenes
var free_scene = true 

var base_camera:Camera2D = null

var game : Game

var current_transition


func _ready():
	root = get_tree().get_root()

func change_scene(path):
	# Load the new scene.
	self.next_scene = ResourceLoader.load(path).instance()
	call_deferred("_deferred_goto_scene")

func change_scene_to_map(path):
	# Load the new scene.
	self.next_ui_state = StateUI.MAP
	current_mode = State.ON_MAP
	self.next_scene = ResourceLoader.load(path).instance()
	self.current_map = self.next_scene
	call_deferred("_deferred_goto_scene")

func change_scene_to_node(node:Node2D):
	# Load the new scene.
	self.next_scene = node
	if next_scene.is_class("Map"):
		current_map = next_scene
		if next_scene.is_class("Mining"):
			next_ui_state = StateUI.MINING
			current_mode = State.MINING
		else:
			next_ui_state = StateUI.MAP
			current_mode = State.ON_MAP
	call_deferred("_deferred_goto_scene")
	
	
func change_scene_to_mining(node:Mining):
	# Load the new scene.
	self.next_scene = node
	self.next_ui_state = StateUI.MINING
	call_deferred("_deferred_goto_scene")


func _deferred_goto_scene():
	current_transition = transition_scene.instance()
	game.add_transition(current_transition)
	get_tree().paused = true
	current_transition.fadeOut()


func mid_transition():
	# It is now safe to remove the current scene
	if free_scene:
		game.free_map()
	
	
	get_tree().paused = false
	current_scene = self.next_scene
	
	if current_scene.has_method("init"):
		var finished = current_scene.init()
		while not finished:
			finished = current_scene.init()
			update_transition_progress(current_scene.get_progress())
			yield(get_tree(), "idle_frame")
	
	get_tree().paused = true
	
	# unpause tree to correctly update player position
	get_tree().paused = false
	game.set_map(current_scene)
	yield(get_tree(), "idle_frame")
	get_tree().paused = true
	
	game.clear_ui()
	game.set_ui_state(next_ui_state)
	current_transition.mid_out()


func finish_transition():
	game.remove_transition(current_transition)
	game.set_ui_state(next_ui_state)
	if next_ui_state == StateUI.MAP:
		current_map = game.get_current_map()
	free_scene = true
	get_tree().paused = false
	current_transition.call_deferred("queue_free")

func update_transition_progress(percentage : int):
	current_transition.update_loading_percentage(percentage)
