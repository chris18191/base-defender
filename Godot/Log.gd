extends Node

func error(s : String):
	print("[Error] " + s)
	
func warn(s : String):
	print("[Warning] " + s)
	
func info(s : String):
	print("[Info] " + s)
