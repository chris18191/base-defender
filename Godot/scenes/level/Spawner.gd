extends Area2D
class_name Spawner

export (PackedScene) var Enemy = null
export (float) var wait_time = 1

export (int) var max_enemy_life = 100
export (int) var min_enemy_life = 20

export (int) var enemies_at_start = 1
export (int) var max_enemy_count = 5

var spawn_area : Rect2

var enemies = 0

func _enemy_died():
	enemies -= 1

func spawn_enemy(pos:Vector2):
	var e = Enemy.instance()
	e.position = pos
	e.max_health = rand_range(min_enemy_life, max_enemy_life)
	e.connect("died", self, "_enemy_died")
	add_child(e)

func _on_Timer_timeout():
	generateEnemy()

func generateEnemy():
	if enemies >= max_enemy_count:
		return
	var pos:Vector2 = spawn_area.size
	pos.x = rand_range(0, pos.x)
	pos.y = rand_range(0, pos.y)
	if check_position(pos):
		spawn_enemy(pos + spawn_area.position)
		enemies += 1

func check_position(_pos:Vector2) -> bool:
	#spawn_area.get_cellv(pos) != -1
	return true
		

func _ready():
	var first_child : CollisionShape2D = get_node("Area")
	if not first_child.is_class("CollisionShape2D"):
		Log.error("[Spawner] error: first child is not a CollisionShape2D!")
	if not first_child.shape.is_class("RectangleShape2D"):
		Log.error("[Spawner] error: collision shape is no RectangleCollisionShape!")
	else:
		spawn_area = Rect2(first_child.position, first_child.shape.extents)
	for i in enemies_at_start:
		generateEnemy()
