extends Map
class_name Mining

var background : TileMap = null
var foreground : TileMap = null
	

func set_map(f:TileMap, b:TileMap):
	if background in self.get_children():
		remove_child(background)
	if foreground in get_children():
		remove_child(foreground)
	
	self.foreground = f;
	self.background = b;
	add_child(background)
	add_child(foreground)

func get_spawn() -> Position2D:
	var pos : Vector2 = background.get_used_rect().position * background.cell_size
	pos += background.get_used_rect().size * background.cell_size / 2
	spawn = Position2D.new()
	spawn.position = pos
	return spawn


func _input(event):
	if event.is_action_released("stopMining") and Global.current_mode == Global.State.MINING:
		GlobalMining.stop_mining()
