extends Node2D

export (String) var  sceneToGo = null

func _on_Area2D_area_entered(area):
	var parent = area.get_parent()
	if area.get_parent().is_class("Base"):
		if sceneToGo != null:
			Global.change_scene(sceneToGo)
