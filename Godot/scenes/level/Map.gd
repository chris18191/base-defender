extends Node2D
class_name Map

onready var spawn : Position2D = $Spawn

func _ready():
	Global.current_map = self

func is_class(c : String):
	if c == "Map":
		return true
	return false

func get_spawn() -> Position2D:
	return spawn

func get_region_foreground(rec : Rect2) -> TileMap:
	return _get_region_map(rec, $Maps/Foreground)

func get_region_background(rec : Rect2) -> TileMap:
	return _get_region_map(rec, $Maps/Background)

func _get_region_map(rec : Rect2, map : TileMap) -> TileMap:
	# rect in tile cells
	var res : TileMap = TileMap.new()
	res.tile_set = map.tile_set
	res.cell_size = map.cell_size
	for x in range(rec.position.x, rec.position.x + rec.size.x):
		for y in range(rec.position.y, rec.position.y + rec.size.y):
			res.set_cell(x, y, map.get_cell(x,y));
	res.update_bitmask_region()
	return res


func get_position_in_cells(pos : Vector2) -> Vector2:
	return $Maps/Background.world_to_map(pos)
