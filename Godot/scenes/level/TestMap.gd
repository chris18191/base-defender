extends "res://scenes/level/Map.gd"

var timer = 0
var Enemy = preload("res://scenes/actors/enemies/TestEnemy.tscn")
var MapGenerator = preload("res://tools/MapGenerator.tscn").instance()

func _ready():
	self.set_process(true)

func _process(delta):
	timer += delta
	if timer > 3:
		var pos = $TestBase.position #$Background.get_used_rect().size * $Background.cell_size
		pos.x += rand_range(-200, 200)
		pos.y += rand_range(-200, 200)
		spawnEnemy(pos)
		timer -= 3

func spawnEnemy(pos:Vector2):
	var e = Enemy.instance()
	e.position = pos
	e.health = rand_range(100, 400)
	add_child(e)
