extends "res://scenes/level/Map.gd"
class_name RandomMap

export (OpenSimplexNoise) var noise = OpenSimplexNoise.new()
export (TileSet) var tile_set = null

export (Vector2) var map_size = Vector2(800, 600)
export (Vector2) var cell_size = Vector2(16, 16)

export (Array, int) var background_tiles = []
export (Array, int) var foreground_tiles = []
export (Array, int) var decoration_tiles = []
export (Array, int) var outside_tiles = []

export (float, 0, 1) var prob_background = 0.5
export (float, 0, 1) var prob_foreground = 0.1
export (Vector2) var prob_decoration = Vector2(0.4, 0.5)

export (int) var border_width = 2

var _initialized = false
var _initialized_maps = false
var _initialized_inside = false
var _initialized_outside = false

var _x_progress = 0

signal finished_init

func _ready():
	if not _initialized:
		var status = _createMap()
		while not status:
			status = _createMap()

func get_progress() -> int:
	return (_x_progress*100/map_size.x)

# returns 1 if the generation is finished otherwise
func init() -> int:
	if _initialized:
		return 1
	if _createMap() == 1:
		_initialized = true
		emit_signal("finished_init")
		return 1
	
	return 0

func _createMap() -> int:
	if _initialized:
		return 1
	randomize()
	if not _initialized_maps:
		initMaps()
	if not _initialized_inside:
		generateInside()
		return 0
	if not _initialized_outside:
		generateOutside()
		return 0
	return 1

func initMaps():
	for map in $Maps.get_children():
		map.tile_set = tile_set
		map.cell_size = cell_size
	_initialized_maps = true

func generateInside():
	for _i in range(10):
		if _x_progress < map_size.x:
			for y in map_size.y:
				$Maps/Background.set_cell(_x_progress,y, _random_tile(background_tiles))
				var a = noise.get_noise_2d(_x_progress, y)
				if a < prob_foreground:
					$Maps/Foreground.set_cell(_x_progress, y, _random_tile(foreground_tiles))
				if a > prob_decoration.x and a < prob_decoration.y and $Maps/Foreground.get_cell(_x_progress,y) == -1:
					$Maps/Decoration.set_cell(_x_progress,y, _random_tile(decoration_tiles))
			_x_progress += 1
	if _x_progress < map_size.x:
		return
	
	$Maps/Decoration.update_bitmask_region()
	$Maps/Foreground.update_bitmask_region()
	$Maps/Background.update_bitmask_region()
	_initialized_inside = true

func generateBackground():
	for x in map_size.x:
		for y in map_size.y:
			$Maps/Background.set_cell(x,y, _random_tile(background_tiles))
				
	$Maps/Background.update_bitmask_region()

func generateForeground():
	for x in map_size.x:
		for y in map_size.y:
			var a = noise.get_noise_2d(x,y)
			if a < prob_foreground:
				$Maps/Foreground.set_cell(x,y, _random_tile(foreground_tiles))
	$Maps/Foreground.update_bitmask_region()

func generateDecoration():
	for x in map_size.x:
		for y in map_size.y:
			var a = noise.get_noise_2d(x,y)
			if a > prob_decoration.x and a < prob_decoration.y and $Maps/Foreground.get_cell(x,y) == -1:
				$Maps/Decoration.set_cell(x,y, _random_tile(decoration_tiles))
	$Maps/Decoration.update_bitmask_region()

func generateOutside():
	for x in range(-border_width, map_size.x + border_width):
		for y in range(-border_width, 0):
			$Maps/Outside.set_cell(x, y, _random_tile(outside_tiles))
		for y in range(map_size.y, map_size.y + border_width):
			$Maps/Outside.set_cell(x, y, _random_tile(outside_tiles))
	for y in range(-border_width, map_size.y + border_width):
		for x in range(-border_width, 0):
			$Maps/Outside.set_cell(x, y, _random_tile(outside_tiles))
		for x in range(map_size.x, map_size.x + border_width):
			$Maps/Outside.set_cell(x, y, _random_tile(outside_tiles))
	$Maps/Outside.update_bitmask_region()
	_initialized_outside = true

func _random_tile(list:Array) -> int:
	return list[randi() % list.size()]
