extends Control

class_name Transition

onready var progress : TextureProgress = $TextureProgress

var second_half_animation:FuncRef = null

func _ready():
	$Background.visible = false
	progress.visible = false
	progress.value = 0

func fadeOut():
	$Background.visible = true
	$AnimationPlayer.play("fadeOut")
	self.second_half_animation = funcref(self, "fadeIn")
	
func fadeIn():
	$AnimationPlayer.play("fadeIn")

func mid_in():
	progress.visible = true
	Global.mid_transition()

func mid_out():
	progress.visible = false
	self.second_half_animation.call_func()

func finished():
	$Background.visible = false
	Global.finish_transition()

func update_loading_percentage(percentage : int):
	progress.value = percentage
