extends HBoxContainer

class_name StatBar

export (bool) var show_label = false
export (bool) var is_child_of_agent = true
export (Texture) var bar_texture = null

onready var bar : TextureProgress = $Bar
onready var label : RichTextLabel = $NumericDisplay

var agent : Actor

func _ready():
	if is_child_of_agent:
		agent = get_owner()
		update_bar()
	if bar_texture == null:
		print_debug("no bar texture set in health bar")
	else:
		bar.texture_progress = bar_texture
	if not show_label:
		label.queue_free()

func set_agent(a : Actor):
	self.agent = a
	update_bar()

func update_label():
	pass

func update_bar():
	pass
