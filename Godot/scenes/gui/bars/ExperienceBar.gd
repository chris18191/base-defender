extends StatBar

class_name ExperienceBar

func _ready():
	bar.max_value = 100 # TODO calculate exp for level
	._ready()

func set_agent(agent:Actor):
	self.agent = agent
	bar.max_value = 100 # TODO
	if agent.connect("exp_gained", self, "update_bar"):
		print_debug("[ExperienceBar] could not connect exp gained signal with actor")
	update_bar()

func update_bar():
	# modulates the health bar on a scale between green and red
	bar.value = agent.experience
	
	if show_label:
		update_label()
