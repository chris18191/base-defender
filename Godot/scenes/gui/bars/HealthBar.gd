extends StatBar

class_name HealthBar

func _ready():
	._ready()


func set_agent(agent:Actor):
	self.agent = agent
	bar.max_value = agent.max_health
	if agent.connect("took_damage", self, "update_bar"):
		print_debug("could not link signal to health bar")
	update_bar()

func update_bar():
	# modulates the health bar on a scale between green and red
	if agent.health <= 0:
		bar.value = 0
		return
		
	var ratio =  float(agent.health)/agent.max_health
	
	if ratio >= 1:
		bar.value = bar.max_value
		ratio = 1
	else:
		bar.value = agent.health
	bar.tint_progress = Color.from_hsv(0.3*ratio, 1.0, 1.0)
	
	if show_label:
		update_label()
