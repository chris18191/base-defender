extends KinematicBody2D

class_name Actor

signal took_damage
signal died
signal exp_gained

export (int) var attack_damage
export (float) var attack_speed = 1
export (int) var max_health = 100
export (int) var movement_speed = 1
var health : int = max_health

# how much exp an actor drops when killed
export (int) var experience_drop = 10

# determines how much exp an actor has
var experience : int

func give_exp(new_exp : int):
	self.experience += new_exp
	emit_signal("exp_gained")

func die():
	emit_signal("died")
	queue_free()

func take_damage(damage:int, attacker:Actor):
	if damage >= self.health:
		self.health = 0
		attacker.give_exp(self.experience_drop)
		die()
	else:
		self.health -= damage
	emit_signal("took_damage")
