extends Actor

class_name Enemy

var current_target = null
var target_nearby = false
var can_attack = true

onready var current_behaviour : Node = get_node("Behaviour/RandomWalk")


# needed to check for enemy in tower script
func is_class(c:String):
	if c == "Enemy":
		return true
	return false

func attack(target: Actor):
	target.take_damage(self.attack_damage, self)
	$AttackCooldown.start()
	can_attack = false

func move_towards(target:Vector2, delta:float):
	$Body.look_at(target)
	var direction = Vector2(cos($Body.rotation), sin($Body.rotation))
	var _collision = move_and_collide(direction*movement_speed*delta)

func _physics_process(delta):
	current_behaviour.act(delta)


func _ready():
	health = max_health
	$AttackCooldown.wait_time = attack_speed
	set_physics_process(true)

func _on_View_body_entered(body):
	if body.is_class("Base"):
		if current_target == null:
			current_target = body


func _on_View_body_exited(body):
	if body.is_class("Base"):
		current_target = null


func _on_Attack_body_entered(body):
	if body.is_class("Base"):
		target_nearby = true


func _on_Attack_body_exited(body):
	if body.is_class("Base"):
		target_nearby = false


func _on_AttackCooldown_timeout():
	can_attack = true
