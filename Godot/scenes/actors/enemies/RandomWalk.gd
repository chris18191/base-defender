extends Node

export (float, 0, 1) var chance_idle = 0.5
onready var agent : Enemy = get_owner()

func act(delta):
	if agent.target_nearby:
		if agent.can_attack:
			agent.attack(agent.current_target)
	elif agent.current_target:
		agent.move_towards(agent.current_target.position, delta)
	else:
		# walk or idle
		pass


