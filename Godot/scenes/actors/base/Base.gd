extends Actor
class_name Base

export (float) var rotationSpeed

var direction:Vector2 = Vector2(0,-1)

func _ready():
	Global.base_camera = get_node("Camera2D")
	set_physics_process(true)

func is_class(c:String):
	if c == "Base":
		return true
	return false

func _physics_process(delta):
	if Global.current_mode != Global.State.ON_MAP:
		return
	direction = Vector2(cos(self.rotation), sin(self.rotation))
	var velocity = 0
	if Input.is_action_pressed("forward"):
		velocity = movement_speed * delta
	elif Input.is_action_pressed("backward"):
		velocity = movement_speed * delta * -0.5
	if Input.is_action_pressed("rotate_left"):
		if Input.is_action_pressed("rotate_right"):
			velocity = movement_speed * delta
		else:
			self.set_rotation(self.get_rotation() - rotationSpeed*delta)
	elif Input.is_action_pressed("rotate_right"):
		self.set_rotation(self.get_rotation() + rotationSpeed*delta)
	var _collision = move_and_collide(direction*velocity)

func die():
	print("GAME OVER")
