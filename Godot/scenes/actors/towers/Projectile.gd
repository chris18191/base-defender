extends Sprite

class_name Projectile

export (float) var speed = 10

var damage: float
var target: Actor
var attacker: Actor

func _ready():
	pass

func _physics_process(delta):
	if self == null:
		return
	if target == null:
		call_deferred("explode")
		return
	self.rotation = target.position.angle_to_point(self.position) + 90
	self.position += (target.position - self.position).normalized() * speed * delta

func hit():
	pass

func explode():
	#$Collision/Shape.disabled = true
	queue_free()

func _on_Collision_area_entered(area):
	if area.get_parent().is_class("Enemy"):
		area.get_parent().take_damage(self.damage, self.attacker)
		explode()


func _on_Collision_body_entered(body):
	if body.is_class("Enemy"):
		# TODO attacker == NIL !
		body.take_damage(self.damage, self.attacker)
		explode()
