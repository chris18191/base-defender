extends Node2D

export(PackedScene) var Projectile

export(float) var shootingSpeed = 1.0
export(float) var damage = 50.0
export(float) var attacKTime = 1.0		# once per second

var attacker : Node2D

var enemiesInSight : Array = []
var readyToShoot : bool = true

func _ready():
	attacker = get_owner().get_owner()
	$Timer.wait_time = self.attacKTime


func _process(_delta):
	if len(enemiesInSight) > 0:
		look_at(enemiesInSight[0].position)
		if readyToShoot:
			shoot()

func shoot():
	var p = Projectile.instance()
	p.target = enemiesInSight[0]
	p.damage = self.damage
	p.attacker = attacker
	p.position = self.get_global_position()
	# add bullet to the global map scene
	Global.current_map.add_child(p)
	readyToShoot = false
	$Timer.start()

func _shoot():
	enemiesInSight[0].takeDamage(self.calc_damage())
	readyToShoot = false
	$Timer.start()

func _on_Timer_timeout():
	self.readyToShoot = true

func calc_damage():
	return self.damage


func _on_Area_body_entered(body):
	if body.is_class("Enemy"):
		enemiesInSight.append(body)
	look_at(body.position)


func _on_Area_body_exited(body):
	if body.is_class("Enemy"):
		enemiesInSight.erase(body)
